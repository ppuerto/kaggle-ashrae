# Kaggle ASHRAE

https://www.kaggle.com/c/ashrae-energy-prediction/overview

## Installation

- ```git clone git@gitlab.com:ppuerto/kaggle-ashrae.git```
- ```cd kaggle-ashrae```
- ```virtualenv -p python3 venv```
- ```source venv/bin/activate```
- ```pip install -r requirements```
- ```ipython kernel install --name "Kaggle" --user```
- Download data from Kaggle and put it in `data/ashrae-energy-prediction`

## Run
- ```jupyter notebook``` 